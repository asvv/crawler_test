import sys
import requests
from bs4 import BeautifulSoup

def soup_find(soup, tag, attr, attr_val):
	if attr is 0:
		return soup.find(tag)
	return soup.find(tag, {attr : attr_val})

def soup_find_all(soup, tag, attr, attr_val):
	if attr is 0:
		return soup.find_all(tag)
	return soup.find_all(tag, {attr : attr_val})

def make_dic(tag, index, id):
	dic = {}
	dic[index] = {
		"ID" : id,
		"Title" : tag.text,
		"href" : tag.get('href')
	}
	return dic

def zangsisi_parser(url, request):
	# Zangsisi Index site parse
	html = request.text
	soup = BeautifulSoup(html, 'html.parser')
	a_list = soup_find(soup, "div", "id", "manga-list").find_all("a")
	
	print("Cartoon List---------------------------")
	m_dic = {}
	for index, p_tag in enumerate(a_list):
		page_id = p_tag.get('href').split("page_id")[1]
		m_dic = make_dic(p_tag, index, page_id )
		
		# Zangsisi Cartoon List
		print(m_dic[index]['Title'])
		
	# Download
	cartoon_name = input()
	for index, tag in enumerate(m_dic):
		if m_dic[index]["Title"] is cartoon_name:
			mana = m_dic[index]
			c_req = requests.get(url + "/?page_id=" + mana["ID"])
			c_soup = BeautifulSoup(c_req.text, "html.parser")
			
			c_a = soup_find_all(c_soup, "a", "class", "tx-link")
			part_dic = {}
			for idx, c_tag in enumerate(c_a):
				p_id = c_tag.get('href').split("?p=")[1]
				part_dic = make_dic(c_tag, idx, p_id)
				
	
	
	
# url => zangsisi.net
def real_main():
	# Check argv
	if len(sys.argv) is 1:
		print("python crawler.py [url]")
		return
	
	# Get url
	url = 'http://' + sys.argv[1]
	request = requests.get(url)
	
	# Check status code
	if request.status_code is not 200:
		print("Response : " + request.status_code)
		return
	
	# Zangsisi parse
	zangsisi_parser(url, request)
	

if __name__ == "__main__":
	real_main()
